//
//  City.swift
//  VDemo
//
//  Created by Xavier Paez on 1/25/17.
//  Copyright © 2017 XavierPaez. All rights reserved.
//

import UIKit
import SwiftyJSON
import Foundation

class City: NSObject {
    
   	public var name : String?
    public var image : String?
    public var imageDownloaded: UIImageView?
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [City]
    {
        var models:[City] = []
        for item in array
        {
            models.append(City(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        
        name = dictionary["name"] as? String
        image = dictionary["image"] as? String
    }
    
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.name, forKey: "name")
        dictionary.setValue(self.image, forKey: "image")
        
        return dictionary
    }

}


