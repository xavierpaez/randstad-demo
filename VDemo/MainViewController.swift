//
//  MainViewController.swift
//  VDemo
//
//  Created by Xavier Paez on 1/26/17.
//  Copyright © 2017 XavierPaez. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftyJSON
import Foundation


class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchDisplayDelegate{
    
    var cities = [City]()
    var filteredCities = [City]()
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var cache = NSCache<NSString, UIImage>()
    
    @IBOutlet weak var citiesTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        citiesTableView.dataSource = self
        citiesTableView.delegate = self
        searchBar.delegate = self
        
        parseJson()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return filteredCities.count
    }
    
    /*!
     In order to optimized the display of the tableview Kingfisher library is being used. 
     Asynchronous image downloading and caching
     */
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
        
        let city = filteredCities[indexPath.row]
        cell.cityNameLabel.text = city.name
        cell.imgView.image = UIImage(named: "placeholder")
       
        
        if city.image != nil{
            let url = URL(string: city.image!)
            cell.imgView.kf.indicatorType = .activity
            cell.imgView.kf.setImage(with: url)
            city.imageDownloaded?.kf.setImage(with: url)
        }

        /*!
        if let image = cache.object(forKey: (city.image as AnyObject) as! NSString)
        {
            cell.imgView.image = image
        }
        else{
            if let imgString = city.image{
                DispatchQueue.global().async {
                    let data = NSData(contentsOf: URL(string: imgString)!)
                    DispatchQueue.main.async {
                        if let updateCell = tableView.cellForRow(at: indexPath) as? TableViewCell {
                            let image = UIImage(data: data as! Data)
                            city.imageDownloaded = image
                            updateCell.imgView.image = image
                            self.cache.setObject(image!, forKey: imgString as NSString)
                        }
                    }
                    
                }
            }
        }
 */
        
        return cell
    }
 

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 44.0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let cityViewController = segue.destination as! CityViewController
        if segue.identifier == "citySegue" {
            let indexPath = self.citiesTableView.indexPathForSelectedRow
            cityViewController.city = filteredCities[indexPath!.row]
        }
        
    }

    
    /*!
    SwiftyJson added to parse the local Json File 
    */
    func parseJson(){
        if let jsonPath = Bundle.main.path(forResource: "cities", ofType: "json") {
            let jsonData = NSData(contentsOfFile: jsonPath) as NSData!
            let json = JSON(data: jsonData as! Data, options: JSONSerialization.ReadingOptions.mutableContainers, error: nil)
            if let resultArray = json.arrayObject {
                for item in resultArray {
                    let data = item as! NSDictionary
                    cities.append(City(dictionary: data)!)
                }
            }
        }
        cities.sort{$0.name! < $1.name!}
        filteredCities = cities
    }
    
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            filteredCities = cities
        } else{
        filteredCities = self.cities.filter({ (city : City) -> Bool in
            return (city.name?.contains(searchText))!
        })
        }
        
        alphabeticalSort()
      
    }
    
    @IBAction func sortSegmentedControl(_ sender: UISegmentedControl) {
        alphabeticalSort()
    }
    
    func alphabeticalSort(){
        if segmentedControl.selectedSegmentIndex == 0 {
            filteredCities.sort{$0.name! < $1.name!}
            self.citiesTableView.reloadData()
        }
        else
        {
            filteredCities.sort{$0.name! > $1.name!}
            self.citiesTableView.reloadData()
        }

    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
