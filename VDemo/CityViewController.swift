 //
//  CityViewController.swift
//  VDemo
//
//  Created by Xavier Paez on 1/26/17.
//  Copyright © 2017 XavierPaez. All rights reserved.
//

import UIKit

class CityViewController: UIViewController {

    var city : City?
    
    @IBOutlet weak var cityImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: city!.image!)
        cityImage.kf.setImage(with: url)
        self.title = city?.name
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
